module.exports = {
  setupTestFrameworkScriptFile: './test-setup.js',
  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95
    }
  },
  moduleNameMapper: {
    "^.+\\.css$": "identity-obj-proxy",
    "^.+\\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/src/__mocks__/fileMock.js"
  }
};
