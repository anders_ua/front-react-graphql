import React, { Component } from 'react';
import PropTypes from 'prop-types';

const { createApolloFetch } = require('apollo-fetch');

export class App extends Component {
  constructor() {
    super();

    this.state = {
      data: null,
    }

    this.getData = this.getData.bind(this);
  }

  componentDidMout() {

  }

  getData() {
    const fetch = createApolloFetch({
      uri: 'http://localhost:3000/graphq',
    });

    fetch({
      query: '{ books { title }}',
    }).then(res => {
      console.log(res.data);
    });

    // You can also easily pass variables for dynamic arguments
    // fetch({
    //   query: `query PostsForAuthor($id: Int!) {
    //   author(id: $id) {
    //     firstName
    //     posts {
    //       title
    //       votes
    //     }
    //   }
    // }`,
    //   variables: { id: 1 },
    // }).then(res => {
    //   console.log(res.data);
    // });

    // {
    //   books {
    //   _id
    //   title
    // }
    // }
  }

  render() {
    return (<div><button onClick={this.getData}> Fetch </button></div>);
  }
}